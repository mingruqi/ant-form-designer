import Vue from 'vue'
import App from './App.vue'
import router from '@/router'
import './permission'
import VueRouter from 'vue-router'
import '@/styles/index.css'
import '@/styles/icon.css'
import message from 'ant-design-vue/lib/message'
import modal from 'ant-design-vue/lib/modal'

Vue.use(message)
Vue.use(modal)
Vue.prototype.$confirm = modal.confirm
Vue.prototype.$message = message
Vue.prototype.$info = modal.info
Vue.prototype.$success = modal.success
Vue.prototype.$error = modal.error
Vue.prototype.$warning = modal.warning

import { service } from '@/utils/service.js'
import store from './store/'
import lt from 'lazyit-tools'
import 'lazyit-tools/lib/lazyit-tools.css'

Vue.config.productionTip = false
Vue.use(VueRouter)
Vue.use(lt)

Vue.prototype.$axios = service

import { formatDate } from '@/utils/tools'
Vue.prototype.$formatDate = formatDate

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
